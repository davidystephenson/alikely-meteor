# alikely

Alikely is a web application written in Meteor that helps people do things together.

## Installation

1. Follow the [official instructions](https://www.meteor.com/install) for installing Meteor.
1. Set the `MONGO_URL` environment variable to the correct URI.
1. Create a `config` directory, and inside create a file named `settings.json` with the following content:

        {
          "tmdbKey": "<key>",
          "musixmatchKey": "<key>"
        }

## Execution

1. Run `meteor --settings config/settings.json`. You may want to set a different port from the default 3000 by using the `--port` flag.
