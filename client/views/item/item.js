Template.item.onCreated(function() {
  var self = this;

  self.expanded = new ReactiveVar(false);
  self.previewData = new ReactiveVar({});

  listSources[self.data.source].getPreview(
    self.data.sourceId,
    function (data) { self.previewData.set(data); }
  );
});

Template.item.events({
  'click .delete-item': function(e) {
    e.preventDefault();

    var self = this;

    var list = Template.parentData();

    Meteor.call('deleteItem', self._id, list._id, function(error, result) {
      if (error) console.log(error);
    });
  },

  'click .toggleExpanded': function(event, template) {
    template.expanded.set(!template.expanded.get());
  },
});

Template.item.helpers({
  canPreview: function() {
    var self = this;

    if (self.source === 'Custom') return false;
    return true;
  },

  isExpanded: function() { return Template.instance().expanded.get(); },

  isOwner: function() {
    var self = this;

    var list = Template.parentData();

    return Meteor.userId() === list.userId;
  },

  previewData: function() { return Template.instance().previewData.get(); },
});
