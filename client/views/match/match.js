Template.match.onCreated(function() {
  var self = this;

  self.expanded = new ReactiveVar(false);
  
  self.previewData = new ReactiveVar({});

  listSources[self.data.item.source].getPreview(
    self.data.item.sourceId,
    function (data) { self.previewData.set(data); }
  );

  self.subscribe('lists');
});

Template.match.events({
  'click .toggleExpanded': function(event, template) {
    template.expanded.set(!template.expanded.get());
  },
});

Template.match.helpers({
  canPreview: function() {
    var self = this;

    if (self.item.source === 'Custom') return false;
    return true;
  },

  getEmail: function() {
    var self = this;

    var user = Meteor.users.findOne(self.list.userId);

    return user.emails[0].address;
  },

  isExpanded: function() { return Template.instance().expanded.get(); },

  listName: function() {
    var self = this;

    var listId = self.listIds[0];

    return Lists.findOne(listId).name;
  },

  otherEmail: function() {
    var self = this;

    for (var i = 0; i < self.userIds.length; i ++) {
    if (self.userIds[i] != Meteor.userId()) {
        var user = Meteor.users.findOne(self.userIds[i]);

        return user.emails[0].address;
      }
    }
  },

  otherId: function() {
    var self = this;

    for (var i = 0; i < self.userIds.length; i ++) {
      if (self.userIds[i] !== Meteor.userId()) {
        return self.userIds[i];
      }
    }
  },

  previewData: function() { return Template.instance().previewData.get(); },
});
