Template.matches.onCreated(function() {
  var self = this;

  self.subscribe('items');
  self.subscribe('lists');
});

var getMatches = function(currentUserId) {
  var self = this;

  var pageUserId = Router.current().params._id;

  var lists = Lists.find({ userId: pageUserId});

  matches = [];

  lists.forEach(function(list) {
    var items = Items.find({ _id: { $in: list.itemIds } });

    items.forEach(function(item) { 
      matchingLists = Lists.find({ 
        name: list.name,
        itemIds: { $in: [item._id] },
        userId: { $ne: pageUserId }
      });

      console.log('matchingLists test:', matchingLists.fetch());

      matchingLists.forEach(function(matchingList) {
        matches.push({
          list: matchingList,
          item: item,
        });
      });
    });
  });

  return matches;
};

Template.matches.helpers({
  matches: function() {
    var self = this;

    var pageUserId = Router.current().params._id;

    var lists = Lists.find({ userId: pageUserId});

    matches = [];

    lists.forEach(function(list) {
      var items = Items.find({ _id: { $in: list.itemIds } });

      items.forEach(function(item) { 
        matchingLists = Lists.find({ 
          name: list.name,
          itemIds: { $in: [item._id] },
          userId: { $ne: pageUserId }
        });

        matchingLists.forEach(function(matchingList) {
          matches.push({
            list: matchingList,
            item: item,
          });
        });
      });
    });

    return matches;
  },
});

