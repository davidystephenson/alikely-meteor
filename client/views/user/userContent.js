Template.userContent.helpers({
  content: function (name) { 
    var content = Router.current().params.content;

    if (name === content) return 'active';
  }
})
