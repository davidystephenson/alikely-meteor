Template.following.helpers({
  following: function() {
    var self = this;

    if (self.following) {
      var following = Meteor.users.find({ _id: { $in: self.following } });

      if (following.count() === 0) return false;

      return following;
    }
  },

  getEmail: function() {
    var self = this;

    return self.emails[0].address;
  },
});
