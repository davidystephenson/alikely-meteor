Template.followers.helpers({
  followers: function () {
    var self = this;

    var followers = Meteor.users.find({ following: { $in: [self._id] } });

    if (followers.count() === 0) return false;

    return followers;
  },

  getEmail: function() {
    var self = this;

    return self.emails[0].address;
  },
});
