Template.userSidebar.onCreated(function () {
  var instance = this;

  instance.subscribe('users');

  instance.userId = new ReactiveVar();
  instance.autorun(function() {
    var userId = Router.current().params._id;
    instance.userId.set(userId);
  });

  instance.user = function() { return Meteor.users.findOne(instance.userId.get()) };
});

Template.userSidebar.helpers({
  canFollow: function() {
    var instance = Template.instance();

    var pageUser = instance.user();

    // Check that the user is logged in
    if (Meteor.userId()) {

      // Check that this is not the user's own description
      if (Meteor.userId() !== pageUser._id) {

        // Check that the user is not already following the user.
        var currentUser = Meteor.users.findOne(Meteor.userId());

        if (
          currentUser &&
          currentUser.following &&
          currentUser.following.indexOf(pageUser._id) === -1
        ) return true;
      }
    }
    return false;
  },

  canUnfollow: function() {
    var instance = Template.instance();

    var pageUser = instance.user()

    // Check that the user is logged in
    if (Meteor.userId()) {

      // Check that this is not the user's own description
      if (Meteor.userId() !== pageUser._id) {

        // Check that the user is following the user.
        var currentUser = Meteor.users.findOne(Meteor.userId());

        if (
          currentUser &&
          currentUser.following &&
          currentUser.following.indexOf(pageUser._id) !== -1
        ) return true;
      }
    }

    return false;
  },

  getEmail: function() {
    var instance = Template.instance();

    var pageUser = instance.user();

    if (pageUser && pageUser.emails) return pageUser.emails[0].address;
  },

  isActiveClass: function(name) {
    var content = Router.current().params.content;

    if (name === content) return 'active';
  },
});

Template.userSidebar.events({
  'click .follow': function(event, template) {
    event.preventDefault();

    Meteor.call('followUser', template.user()._id);
  },

  'click .unfollow': function(event, template) {
    event.preventDefault();

    Meteor.call('unfollowUser', template.user()._id);
  },
});
