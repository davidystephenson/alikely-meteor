Template.notifications.events({
  'hidden.bs.dropdown': function() {
    Meteor.call('readNotifications', function(err) {
      if (err) console.log(err);
    });
  }
});

Template.notifications.helpers({
  notifications: function() {
    return Notifications.find({ read: false });
  },

  notificationCount: function(){
    return Notifications.find({ read: false }).count();
  }
});
