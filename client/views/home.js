Template.home.onCreated(function() {
  var self = this;

  self.subscribe('users');
});

Template.home.helpers({
  isUser: function() { return Meteor.user(); },

  users: function() { return Meteor.users.find(); },

  getUserEmail: function(user) { return user.emails[0].address; },
});
