Template.header.helpers({
  activeRouteClass: function(route) {
    var current = Router.current();

    var active;
    if (current) {
      active = current.route.getName() === route;
    }

    return active && 'active';
  },

  isMeActiveClass: function() {
    var self = this;

    var current = Router.current().route.path(self);
    var active = current === '/user/' + Meteor.userId();

    return active && 'active';
  },
});
