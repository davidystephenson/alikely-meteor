Template.blog.onCreated(function() {
  var self = this;

  self.subscribe('posts');
  self.subscribe('users');
});

Template.blog.events({
  'submit .add-post-form': function(event, template) {
    var self = this;

    event.preventDefault();

    var title = $(event.target).find('[name=title]').val();
    var text = $(event.target).find('[name=text]').val();

    Meteor.call('addPost', title, text);
  }
});

Template.blog.helpers({
  isAdmin: function() {
    var user = Meteor.user();
    if (user && user.roles) if (user.roles.indexOf('admin') !== -1) {
      return true;
    }
    return false;
  },

  posts: function() {
    return Posts.find();
  },
});
