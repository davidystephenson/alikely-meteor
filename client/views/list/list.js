Template.list.onCreated(function() {
  var self = this;

  self.subscribe('items');
  self.subscribe('users');
});

Template.list.events({
  'click .deleteList': function(e) {
    e.preventDefault();

    var self = this;

    var listId = self._id;

    Meteor.call('deleteList', listId, function(error, result) {
      if (error) console.log(error);
      Router.go('user', { _id: self.userId });
    });
  },
});

Template.list.helpers({
  isOwner: function() {
    var self = this;

    return Meteor.userId() === self.userId;
  },

  items: function() {
    var self = this;

    return Items.find({ _id: { $in: self.itemIds } });
  },
});
