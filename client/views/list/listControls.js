Template.listControls.created = function() {
  this.loading = new ReactiveVar(false);
  this.validInput = new ReactiveVar(false);
};

Template.listControls.events({
  'submit .add-item-form': function(e, template) {
    var self = this;

    e.preventDefault();

    var itemName = $(e.target).find('[name=item]').val();

    if(itemName && itemName !== '') {
      $(e.target)[0].reset();

      template.loading.set(true);

      listSources[self.source].enterItem(itemName, self, function(options) {
        template.loading.set(false);
        if (options) {
          var templateInstance = Template.instance();
          template.parent(2).options.set(options);
        }
      });
    }
  },

  'keyup .item-input': function(e, template)  {
    if ($(e.target).val()) {
      template.validInput.set(true);
    } else {
      template.validInput.set(false);
    }
  },

  'click .option': function(e, template) {
    Template.instance().parent(2).options.set(false);
  }
});

Template.listControls.helpers({
  isLoading: function() {
    return Template.instance().loading.get();
  },

  isValidInput: function() {
    return Template.instance().validInput.get();
  },

  options: function() {
    var options = Template.instance().parent(2).options.get();
    return options;
  },

  optionsAreEmpty: function() {
    return _.isEqual(Template.instance().parent(2).options.get(), []);
  },

  elementName: function() {
    var self = this;

    return listSources[self.source].elementName;
  }
});
