Template.lists.created = function() {
  var self = this;

  self.subscribe('lists');
  self.subscribe('users');

  self.customListName = new ReactiveVar('Custom');
  self.activeListName = new ReactiveVar('Movies');
  self.options = new ReactiveVar(false);
  self.validInput = new ReactiveVar(false);
};

Template.lists.events({
  'click .movies-tab': function(e, template) {
    template.activeListName.set('Movies');
    template.customListName.set('Custom');
    template.options.set(false);
  },

  'click .songs-tab': function(e, template) {
    template.activeListName.set('Songs');
    template.customListName.set('Custom');
    template.options.set(false);
  },

  'click .custom-list': function(e, template) {
    var self = this;

    template.activeListName.set(self.name);
    template.customListName.set(self.name);
    template.options.set(false);
  },

  'click .delete-list': function(e, template) {
    var self = this;

    e.preventDefault();
    e.stopPropagation();

    Meteor.call('deleteList', self._id, function(error, result) {
      if (error) console.log(error);
      else {
        if (
          template.activeListName.get() ===
          template.customListName.get()
        ) {
          template.activeListName.set('Movies');
          template.customListName.set('Custom');
        }
      }
    });
  },

  'keyup .custom-list-input': function(e, template)  {
    if ($(e.target).val()) {
      template.validInput.set(true);
    } else {
      template.validInput.set(false);
    }
  },

  'submit .new-custom-list-form': function(e, template) {
    var self = this;

    e.preventDefault();

    var listName = $(e.target).find('[name=listName]').val();

    if (listName && listName !== '') {
      $(e.target)[0].reset();

      Meteor.call('createList', Meteor.userId(), listName, 'Custom', function(error, result) {
        if (error) console.log(error);
      });
    }
  }
});

Template.lists.helpers({
  activeList: function() {
    var self = this;

    var name = Template.instance().activeListName.get();

    return Lists.findOne({ userId: self._id, name: name });
  },

  canCreate: function() {
    var self = this;

    if (Meteor.userId()) {
      if (self.user) {
        if (self.user._id === Meteor.userId()) return true;
        return false;
      } else return true;
    } else {
      return false;
    }
  },

  customLists: function() {
    var self = this;

    return Lists.find({ userId: self._id, source: 'Custom' });
  },

  customListName: function() {
    return Template.instance().customListName.get();
  },

  isActiveClass: function(name) {
    if (name === Template.instance().activeListName.get()) return 'active';
    else return false;
  },

  isOwnerList: function() {
    var self = this;

    return self.userId === Meteor.userId();
  },

  isOwnerLists: function() {
    var self = this;

    return self._id === Meteor.userId();
  },

  isValidInput: function() {
    return Template.instance().validInput.get();
  },
});
