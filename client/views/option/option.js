Template.option.events({
  'click .option': function(e, template) {
    e.preventDefault();

    var self = this;

    var list = Template.parentData();

    Meteor.call('addItem', self, list._id, function(error, result) {
      if (error) console.log(error);
    });
  },

  'click .external-url': function(e) {
    var self = this;

    window.open(self.externalUrl, '_blank');

    e.stopPropagation();
    e.preventDefault();
  },
});
