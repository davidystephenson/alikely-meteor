Migrations.add({
  version: 7,
  name: 'Added user.roles',

  up: function() {
    Meteor.users.update({}, { $set: { roles: [] } }, { multi: true });
  },

  down: function() {
    Meteor.users.update({}, { $unset: { roles: 1 } });
  },
});
