Migrations.add({
  version: 2,
  name: 'Change created to createdAt.',

  up: function() {
    var lists = Lists.find({ created: { $exists: true } });

    lists.forEach(function(list) {
      Lists.update(
        list._id,
        { 
          $set: { createdAt: list.created },
          $unset: { created: 1 },
        }
      );
    });
  },

  down: function() {
    var lists = Lists.find({ createdAt: { $exists: true } });

    lists.forEach(function(list) {
      Lists.update(
        list._id,
        { 
          $set: { created: list.createdAt },
          $unset: { createdAt: 1 },
        }
      );
    });
  },
});
