Migrations.add({
  version: 3,
  name: 'Added type.',

  up: function() {
    var lists = Lists.find({ type: { $exists: false } });

    lists.forEach(function(list) {
      Lists.update(
        list._id,
        { $set: { type: 'Custom' } }
      );
    });
  },

  down: function() {
    var lists = Lists.find({ type: { $exists: true } });

    lists.forEach(function(list) {
      Lists.update(
        list._id,
        { $unset: { type: 1 } }
      );
    });
  },
});
