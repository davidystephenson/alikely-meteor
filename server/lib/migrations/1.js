Migrations.add({
  version: 1,
  name: 'Change friends to followers and following.',

  up: function() {
    Meteor.users.update(
      { friends: { $exists: true } },
      { 
        $set: { followers: user.friends, following: user.friends },
        $unset: { friends: 1 },
      },
      { multi: true }
    );
  },

  down: function() {
    Meteor.users.update(
      { friends: { $exists: false } },
      { 
        $set: { friends: followers},
        $unset: { followers: 1, following: 1 }
      },
      { multi: true }
    );
  },
});
