Migrations.add({
  version: 9,
  name: 'Changed notification.createdAt to notification.date',

  up: function() {
    Notifications.update(
      { createdAt: { $exists: true } },
      { $rename: { createdAt: 'date' } },
      { multi: true }
    );
  },

  down: function() {
    Notifications.update(
      { date: { $exists: true } },
      { $rename: { date: 'createdAt' } },
      { multi: true }
    );
  },
});
