Migrations.add({
  version: 6,
  name: 'Removed user.followers',

  up: function() {
    Meteor.users.update({}, { $unset: { followers: 1 } }, { multi: true });
  },

  down: function() {
    var allUsers = Meteor.users.find();

    allUsers.forEach(function(allUser) {
      var followers = [];
      var followingUsers = Meteor.users.find({ following: { $in: [allUser._id] } });

      followingUsers.forEach(function(followingUser) {
        followers.push(followingUser._id);
      });

      Meteor.users.update(allUser._id, { $set: { followers: followers } });
    });
  },
});
