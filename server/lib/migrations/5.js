Migrations.add({
  version: 5,
  name: 'Changed Item.listIds to List.itemIds',

  up: function() {
    // Add an itemIds field to each List
    Lists.update(
      {},
      { $set: { itemIds: [] } },
      { multi: true }
    );

    // Find all Items with a listIds field
    var items = Items.find({ listIds: { $exists: true } });

    items.forEach(function(item) {
      // Add the item's _id to the itemIds field of every List
      // in the item's listIds field
      Lists.update(
        { _id: { $in: item.listIds } },
        { $addToSet: { itemIds: item._id } },
        { multi: true }
      );

      // Remove the item's listIds field
      Items.update(
        item._id,
        { $unset: { listIds: 1 } }
      );
    });
  },

  down: function() {
    // Add a listIds field to each Item
    Items.update(
      {},
      { $set: { listIds: [] } },
      { multi: true }
    );

    // Find all lists with an itemIds field
    var lists = Lists.find({ itemIds: { $exists: true } });

    lists.forEach(function(list) {
      // Add the list's _id ot the listIds field of every Item
      // in the list's itemIds field
      Items.update(
        { _id: { $in: list.itemIds } },
        { $addToSet: { listIds: list._id } },
        { multi: true }
      );

      // Remove the list's itemIds field
      Lists.update(
        list._id,
        { $unset: { itemIds: 1 } }
      );
    });
  },
});
