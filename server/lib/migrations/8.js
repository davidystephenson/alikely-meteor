Migrations.add({
  version: 8,
  name: 'Added item.source',

  up: function() {
    Items.update(
      { listName: 'Movies'},
      { $set: { source: 'The Movie Database' } },
      { multi: true }
    );

    Items.update(
      { listName: 'Songs'},
      { $set: { source: 'Musixmatch' } },
      { multi: true }
    );

    Items.update(
      { listName: { $nin: [ 'Songs', 'Movies' ] } },
      { $set: { source: 'Custom' } },
      { multi: true }
    );
  },

  down: function() {
    Items.update(
      {},
      { $unset: { source: 1 } },
      { multi: true }
    );
  },
});
