Migrations.add({
  version: 4,
  name: 'Changed type to source.',

  up: function() {
    var lists = Lists.find({ type: { $exists: true } });

    lists.forEach(function(list) {
      Lists.update(
        list._id,
        { $unset: { type: 1 }, $set: { source: 'None' } }
      );
    });
  },

  down: function() {
    var lists = Lists.find({ type: { $exists: true } });

    lists.forEach(function(list) {
      Lists.update(
        list._id,
        { $unset: { source: 1 }, $set: { type: 'Custom' } }
      );
    });
  },
});
