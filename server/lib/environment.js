// Accounts
Accounts.onCreateUser(function(options, user) {
  user.following = [];
  user.roles = [];

  Meteor.call('createList', user._id, 'Movies', 'The Movie Database');

  Meteor.call('createList', user._id, 'Songs', 'Musixmatch');

  if (options.profile) user.profile = options.profile;

  return user;
});

// Houston
// Houston.add_collection(Meteor.users);
// Houston.add_collection(Houston._admins);
