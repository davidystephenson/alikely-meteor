Meteor.methods({
  getMusixmatch: function (sourceId) {
    check(sourceId, String);

    var url = 'http://api.musixmatch.com/ws/1.1/';
    var mode = 'track.get?track_id=' + sourceId;
    var key = '&apikey=' + Meteor.settings.musixmatchKey;
    var request = url + mode + key;

    var result = HTTP.get(request, { timeout: 30000 });

    var content = JSON.parse(result.content);

    if (result.statusCode === 200) return content.message.body.track;
    else throw new Meteor.Error(result.statusCode, JSON.parse(result.message.body).error);
  },

  getTmdb: function (sourceId) {
    check(sourceId, String);

    var url = 'http://api.themoviedb.org/3/';
    var mode = 'movie/' + sourceId;
    var key = '?api_key=' + Meteor.settings.tmdbKey;
    var request = url + mode + key;

    var result = HTTP.get(request, { timeout: 30000 });

    if (result.statusCode === 200) return JSON.parse(result.content);
    else throw new Meteor.Error(result.statusCode, JSON.parse(result.content).error);
  },

  queryMusixmatch: function (title) {
    check(title, String);

    var url = 'http://api.musixmatch.com/ws/1.1/';
    var mode = 'track.search?page=1&page_size=10&q_track=';
    var sort = '&s_track_rating=desc&apikey=' + Meteor.settings.musixmatchKey;
    var encodedTitle = encodeURI(title);
    var request = url + mode + encodedTitle + sort;

    var result = HTTP.get(request, { timeout: 30000 });

    if (result.statusCode === 200) return JSON.parse(result.content);
    else throw new Meteor.Error(result.statusCode, JSON.parse(result.content).error);
  },

  queryTmdb: function (title) {
    check(title, String);

    var url = 'http://api.themoviedb.org/3/';
    var mode = 'search/movie?query=';
    var key = '&api_key=' + Meteor.settings.tmdbKey;
    var encodedTitle = encodeURI(title);
    var request = url + mode + encodedTitle + key;

    var result = HTTP.get(request, { timeout: 30000 });

    if (result.statusCode === 200) return JSON.parse(result.content);
    else throw new Meteor.Error(result.statusCode, JSON.parse(result.content).error);
  },
});
