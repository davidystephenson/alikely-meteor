Meteor.publish('items', function() {
  return Items.find({});
});

Meteor.publish('lists', function() {
  return Lists.find();
});

Meteor.publish('matches', function() {
  return Matches.find({ userIds: { $in : [this.userId] } });
});

Meteor.publish('notifications', function() {
  return Notifications.find({ userId: this.userId });
});

Meteor.publish('posts', function() {
  return Posts.find();
});

Meteor.publish('users', function() {
  return Meteor.users.find(
    {},
    { fields: { emails: 1, lists: 1, roles: 1, following: 1, createdAt: 1 } }
  );
});
