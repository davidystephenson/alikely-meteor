listSources = {};

ListSource = function(name) {
  var listSource = {};

  listSource.createItem = function(result, list) {
    var item = Item();

    item.displayText = result;
    item.listName = list.name;
    item.sourceId = result;

    return item;
  };

  listSource.elementName = 'an item';

  listSource.enterItem = function(itemName, list, callback) {
    if (itemName && itemName !== '') {
      listSource.getOptions(itemName, list, function(options) {
        if (options.length === 1) {
          Meteor.call('addItem', options[0], list._id, function(error, result) {
            if (error) console.log(error);
            callback(false);
          });
        } else {
          callback(options);
        }
      });
    } else {
      throw new Meteor.Error('invalid', 'Items must have content.');
    }
  };

  listSource.getOptions = function(itemName, list, callback) {
    var result = listSource.processResult(itemName);
    var item = listSource.createItem(result, list);
    callback([item]);
  };

  listSource.getPreview = function(itemId) { return false; };

  listSource.processResult = function(result) {
    return result;
  };

  listSource.name = name;

  listSource.Option = function(result) {
    var option = {};

    option.text = result;
    
    return option;
  };

  listSources[listSource.name] = listSource;

  return listSource;
};
