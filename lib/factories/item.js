Item = function() {
  var item = {};

  item.displayText = '';
  item.externalUrl = '#';
  item.listName = '';
  item.source = 'Custom';
  item.sourceId = '';

  return item;
};
