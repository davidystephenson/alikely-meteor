Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
});

Router.route('/', {
  name: 'home',
});

Router.route('/blog', {
  name: 'blog',
});

Router.route('/user/:_id', {
  name: 'user',
  data: function() { return Meteor.users.findOne(this.params._id); },
});

Router.route('/user/:_id/:content', {
  name: 'userContent',
  data: function() { return Meteor.users.findOne(this.params._id); },
});
