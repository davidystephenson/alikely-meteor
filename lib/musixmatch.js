var musixmatch = ListSource('Musixmatch');

musixmatch.createItem = function(result, list) {
  var item = Item();

  item.displayText = result.track_name + ' (' + result.artist_name + ')';
  item.externalUrl = 'http://musixmatch.com/track/' + result.track_id;
  item.listName = list.name;
  item.source = 'Musixmatch';
  item.sourceId = result.track_id.toString();

  return item;
};

musixmatch.elementName = 'a song';

musixmatch.getOptions = function(itemName, list, callback) {
  Meteor.call('queryMusixmatch', itemName, function(error, result) {
    if(error) console.log(error);

    results = musixmatch.processResult(result);

    var options = [];
    for (var i = 0; i < results.length; i ++) {
      options.push(musixmatch.createItem(results[i].track, list));
    }

    callback(options);
  });
};

musixmatch.getPreview = function (sourceId, callback) {
  Meteor.call('getMusixmatch', sourceId, function(error, result) {
    if (error) throw new Meteor.Error(error);
    else callback({
      externalUrl: 'http://musixmatch.com/track/' + result.track_id,
      imageUrl: result.album_coverart_100x100, 
      description: result.artist_name,
    });
  });
};

musixmatch.processResult = function(result) {
  return result.message.body.track_list;
};
