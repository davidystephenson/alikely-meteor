var tmdb = ListSource('The Movie Database');

tmdb.createItem = function (result, list) {
  var item = Item();

  var year = '????';

  if (result.release_date && result.release_date !== '') {
    year = result.release_date.slice(0, 4);
  }

  item.displayText = result.title + ' (' + year + ')';
  item.externalUrl = 'https://www.themoviedb.org/movie/' + result.id;
  item.listName = list.name;
  item.source = 'The Movie Database';
  item.sourceId = result.id.toString();

  return item;
};

tmdb.elementName = 'a movie';

tmdb.getOptions = function (itemName, list, callback) {
  Meteor.call('queryTmdb', itemName, function(error, result) {
    if (error) throw new Meteor.Error(error);
    else {
      var results = tmdb.processResult(result);

      var options = [];
      for (var i = 0; i < results.length; i ++) {
        options.push(tmdb.createItem(results[i], list));
      }

      callback(options);
    }
  });
};

tmdb.getPreview = function (sourceId, callback) {
  Meteor.call('getTmdb', sourceId, function(error, result) {
    if (error) throw new Meteor.Error(error);
    else callback({
      externalUrl: 'https://www.themoviedb.org/movie/' + result.id,
      imageUrl: 'http://image.tmdb.org/t/p/w92/' + result.poster_path,
      description: result.overview,
    });
  });
};

tmdb.processResult = function (result) {
  return result.results;
};
