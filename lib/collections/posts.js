Posts = new Mongo.Collection('posts');

Meteor.methods({
  addPost: function(title, text) {
    check(title, String);
    check(text, String);

    if (Meteor.user().roles.indexOf('admin') !== -1) {
      Posts.insert({
        text: text,
        title: title,
        date: new Date(),
      });
    } else {
      throw new Meteor.Error('invalidUser', 'You are not an admin.');
    }
  },
});
