Notifications = new Mongo.Collection('notifications');

Meteor.methods({
  readNotifications: function() {
    Notifications.update({ userId: Meteor.userId() }, { $set: { read: true} }, { multi: true });
  },
});
