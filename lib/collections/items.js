Items = new Mongo.Collection('items');

Items.helpers({
});

Meteor.methods({
  addItem: function(itemData, listId) {
    check(Meteor.userId(), String);
    check(
      itemData,
      {
        displayText: String,
        externalUrl: String,
        listName: String,
        source: String,
        sourceId: String,
      }
    );
    check(listId, String);

    var list = Lists.findOne(listId);

    if (itemData) {
      if (list) {
        // Check if the item's listName matches the list's Name.
        if (list.name === itemData.listName) {
          if (list.userId === Meteor.userId()) {
            item = Items.findOne(itemData);

            if (item) Lists.update(listId, { $addToSet: { itemIds: item._id } });
            else { 
              itemId = Items.insert(itemData);
              Lists.update(listId, { $addToSet: { itemIds: itemId } });
            }
          } else {
            throw new Meteor.Error('access', 'You do not own this list.');
          }
        } else { 
          throw new Meteor.Error('invalid', "The Item's source does not match the List's source.");
        }
      } else {
        throw new Meteor.Error('no-such-list', 'This list does not exist.');
      }
    } else {
      throw new Meteor.Error('invalid', 'That item has no content.');
    }
  },

  deleteItem: function(itemId, listId) {
    check(Meteor.userId(), String);
    check(itemId, String);
    check(listId, String);

    var list = Lists.findOne(listId);

    if (list) {
      if (list.userId === Meteor.userId()) {
        Lists.update(listId, { $pull: { itemIds: itemId } });

        var lists = Lists.find({ itemIds: { $in: [itemId] } }).count();

        if (!(lists)) Items.remove(itemId);
      } else {
        throw new Meteor.Error('access', 'You do not own this list.');
      }
    } else {
      throw new Meteor.Error('no-such-list', 'This list does not exist.');
    }
  },
});
