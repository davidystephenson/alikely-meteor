var Users = Meteor.users;

Users.helpers({
  matches: function() {
    var self = this;

    var lists = Lists.find({ userId: self._id});

    matches = [];

    lists.forEach(function(list) {
      var items = Items.find({ _id: { $in: list.itemIds } });

      items.forEach(function(item) { 
        matchingLists = Lists.find({ 
          name: list.name,
          itemIds: { $in: [item._id] },
          userId: { $ne: self._id }
        });

        matchingLists.forEach(function(matchingList) {
          matches.push({
            list: matchingList,
            item: item,
          });
        });
      });
    });

    return matches;
  },
});

Meteor.methods({
  followUser: function(userId) {
    check(Meteor.userId(), String);
    check(userId, String);

    var userBeingFollowed = Meteor.users.findOne(userId);

    if(userBeingFollowed) {
      if (userBeingFollowed._id === Meteor.userId())
        throw new Meteor.Error('invalid', 'You cannot follow yourself.');

      // Update the user doing the following.
      Meteor.users.update(Meteor.userId(), { $addToSet: { following: userId } });

      // Notify the user being followed.
      var followerEmail = Meteor.users.findOne(Meteor.userId()).emails[0].address;

      var notification = {
        createdAt: new Date(), 
        read: false,
        text: '<strong>' + followerEmail + '</strong> is following you.',
        url: '/user/' + Meteor.userId(),
        userId: userId,
      };

      var existingNotifications = Notifications.find(
        { text: notification.text, userId: notification.userId, read: false }
      );

      var exists = existingNotifications.count() > 0;

      if (!exists)
        var notificationId = Notifications.insert(notification);
      else console.log('Warning: an unread notification with the same text exists.');
    } else {
      throw new Meteor.Error('no-such-user', 'This user does not exist.');
    }
  },

  unfollowUser: function(userId) {
    check(Meteor.userId(), String);
    check(userId, String);

    var userBeingUnfollowed = Meteor.users.findOne(userId);

    if(userBeingUnfollowed) {
      if (userBeingUnfollowed._id === Meteor.userId())
        throw new Meteor.Error('invalid', 'You cannot unfollow yourself.');

      // Update the user doing the following.
      Meteor.users.update(Meteor.userId(), { $pull: { following: userId } });
    } else {
      throw new Meteor.Error('no-such-user', 'This user does not exist.');
    }
  },
});
