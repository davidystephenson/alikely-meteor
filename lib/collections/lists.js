Lists = new Mongo.Collection('lists');

Meteor.methods({
  // The userID must be passed directly to createList so that
  // it can be run in the Accounts.onCreateUser hook, which currently executes
  // before the user is logged in or has been added to the database.
  createList: function(userId, name, source) {
    check(userId, String);
    check(name, String);
    check(source, String);

    var user = Meteor.user();

    if (name) {
      var listWithSameName = Lists.findOne({
        userId: userId,
        name: name,
      });

      if (listWithSameName) {
        throw new Meteor.Error('invalid', 'You already have a list with that name.');
      }

      var list = {
        createdAt: new Date(),
        itemIds: [],
        name: name,
        userId: userId,
        source: source,
      };

      var listId = Lists.insert(list);

      return  { _id: listId };
    } else {
      throw new Meteor.Error('invalid', 'Lists must have a name.');
    }
  },

  deleteList: function(listId) {
    check(Meteor.userId(), String);
    check(listId, String);

    var list = Lists.findOne({ _id: listId });

    if (list.name === 'Movies' || list.name === 'Songs')
      throw new Meteor.Error('invalid', 'You cannot delete a default list.');

    if (list) {
      if (list.userId === Meteor.userId()) {
        for (i = 0; i < list.itemIds.length; i++) {
          var itemId = list.itemIds[i];

          lists = Lists.find({ itemIds: { $in: [itemId] } }).count();

          if (lists < 2) Items.remove(itemId);
        }

        Lists.remove(listId);
      } else {
        throw new Meteor.Error('access', 'You do not own this list.');
      }
    } else {
      throw new Meteor.Error('no-such-list', 'This list does not exist.');
    }
  },
});
